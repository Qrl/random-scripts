<?php

	include_once("simple_html_dom.php");
	$raw_data = file_get_html("http://www.id.ee/");
	$stats_html = $raw_data->find("li.stats ul", 0);
	
    $message = "";
    foreach($stats_html->find("li") as $element):
        if($element->find("strong", 0) === NULL && $element->find("a", 0) === NULL):
			if(strpos($element->plaintext, 'Seisuga') !== false):
				$date = str_replace("Seisuga", "", $element->plaintext);
				$message .= $date . "<br>";
			endif;
        endif;
        if($element->find("strong", 0) !== NULL && $element->find("a", 0) === NULL):
            $field_value = $element->find("strong", 0)->plaintext;
            $field_name = str_replace($field_value, "", $element->plaintext);
            $message .= $field_name . ":" . $field_value . "<br>";
        endif;
    endforeach;

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '/var/www/html/robot/PHPMailer/src/Exception.php';
	require '/var/www/html/robot/PHPMailer/src/PHPMailer.php';
	require '/var/www/html/robot/PHPMailer/src/SMTP.php';

	$to = "";

	$subject = "SK id.ee Monthly Stats";
	$message = '<p>' . $message . '</p>';
	$domain = parse_url("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
	$domain = $domain["host"];
	$headers = 'From: abc123@abc123.ee' . "\r\n" .
				'Reply-To: webmaster@' . $domain . "\r\n" .
				'X-Mailer: PHP/' . phpversion() . "\r\n" . 
				'Content-Type: text/html; charset=ISO-8859-1\r\n';
	$result = mail($to, $subject, $message, $headers);
	if($result){
		echo "Reminder sent";
	}else{
		echo "Reminder not sent";
	}

?>
