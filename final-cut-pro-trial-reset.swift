#!/usr/bin/swift

import Foundation

let path = NSString(string: "~/Library/Application Support/.ffuserdata").expandingTildeInPath
let data = NSKeyedUnarchiver.unarchiveObject(withFile: path) as! NSDictionary
let mutableData = data.mutableCopy() as! NSMutableDictionary

for (key, value) in mutableData {
  if value is NSDate {
    mutableData[key] = Date()
  }
}

NSKeyedArchiver.archiveRootObject(mutableData, toFile: path)

print("You'd better buy it")