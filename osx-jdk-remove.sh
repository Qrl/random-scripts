#!/bin/bash

test_sudo() {
	if ! which sudo>/dev/null; then
		make_fail "You must be in sudoers group."
	fi
}

test_root () {
	if test `id -u` -eq 0; then
		echo "You ran this script as root. DO NOT RUN RANDOM SCRIPTS AS ROOT!"
		exit 2
	fi
}

confirmation () {
	while true; do
		read -p "Do You wish to remove all installed JDK packages? [y/n]" yn
			case $yn in
				[Yy]* ) 
					echo "Removing JDK packages.."
					remove_jdk                
					break;;
				[Nn]* ) echo "Goodye!"; exit;;
				* ) echo "Please answer yes or no.";;
		esac
	done	
}

remove_jdk () {
	sudo rm -fr /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin
	sudo rm -fr /Library/PreferencePanes/JavaControlPanel.prefPane
	sudo rm -fr ~/Library/Application\ Support/Java
	sudo rm -rf /Library/Java/JavaVirtualMachines/jdk*
	status=$?
	if [[ $? -ne 0 ]]; then
		echo -e "\nSomething went wrong, sry ¯\_(ツ)_/¯"
	else
		echo -e "\nJDK packages have been removed, goodbye!"
	fi
}

test_root
test_sudo
confirmation